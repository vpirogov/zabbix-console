import "./style.css";
import Config from "./controllers/config.js";
import { ZaCon } from "./controllers/zacon/zacon.js";


const mb = document.querySelector("div#messageBoard");
const con = new ZaCon(mb, Config.url, Config.user, Config.password);
mb.addEventListener("click", event => {
  if (event.target.dataset.entity &&
    event.target.dataset.value) {
    con.schedule(event.target.dataset.entity, event.target.dataset.value);
  }
  event.stopPropagation();
});
document.body.addEventListener("keydown", (event) => {
  if (event.altKey && event.code == "KeyC") {
    con.destroyAll();
  }
  if (event.altKey && event.code == "KeyG") {
    con.getGroups();
  }
  if (event.altKey && event.code == "KeyW") {
    con.destroyClosest();
  }
  if (event.altKey && event.keyCode == "38") {
    con.moveUp();
  }
  if (event.altKey && event.keyCode == "40") {
    con.moveDown();
  }
  if (event.altKey && event.keyCode == "61") { // Alt++
    con.setGraphHeight(+con.getGraphHeight() + 20);
  }
  if (event.altKey && event.keyCode == "173") { // Alt++
    con.setGraphHeight(+con.getGraphHeight() - 20);
  }
})




document.querySelector("button#test").addEventListener("click", () => {
  con.test();
});
document.querySelector("button#hostgroup").addEventListener("click", () => {
  con.getGroups();
});
document.querySelector("button#ping").addEventListener("click", () => {
  con.ping();
});
document.querySelector("button#login").addEventListener("click", () => {
  con.login();
});
document.querySelector("button#logout").addEventListener("click", () => {
  con.logout();
});
document.querySelector("button#help").addEventListener("click", () => {
  con.help();
});
