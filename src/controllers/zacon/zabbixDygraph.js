function DygraphHistory(graph, id) {
    const zd = {
        data: [], // for dygraph data
        pre: {}, // temporary structure
    };

    // 1. Composing .pre object.
    // keys: time in seconds
    // values: array of dygraph rows [x, y1, y2...]
    graph.items.forEach((item, idx) => {
        item.history.forEach(history => {
            // No need for nanosecond precision, so ignore 'history.ns'.
            let clock = history.clock * 1e3;
            // X-axis data (date).
            if (!zd.pre[clock]) {
                let ts = new Date();
                ts.setTime(clock);
                zd.pre[clock] = [ts];
            }
            // Left padding with nulls.
            while (zd.pre[clock].length < idx + 1) {
                zd.pre[clock].push(NaN);
            }
            // Y-data.
            zd.pre[clock].push(+history.value);

        })
    })
    // Right padding with nulls.
    for (let k in zd.pre) {
        while (zd.pre[k].length < graph.items.length + 1) {
            zd.pre[k].push(NaN);
        }
    }

    // 2. Transform .pre to .data
    // Get sorted array of .pre keys (time in seconds).
    const ticks = Object.keys(zd.pre)
        .sort((a,b) => a - b);
    // Composing .data.
    let prev = [null];

    ticks.forEach(tick => {
        // if (!zd.pre[tick][1] || !zd.pre[tick][2]) return;
        zd.data.push(zd.pre[tick]);
    });
    delete zd.pre;

    // 3. Change y-data nulls to previous values 
    // for fixing ugly graphs in Dygraph 2.1.0.

    // Create previous values array.
    let prevVals = [null]; // X-data "place". For aligning indexes.
    graph.items.forEach(() => prevVals.push(null));

    // Change null.
    zd.data.forEach(row => {
        for (let i = 1; i < row.length; i++) {
            if (!row[i]) {
                row[i] = prevVals[i];
            } else {
                prevVals[i] = row[i];
            }
        }
    })
    
    // 4. Graph options
    zd.options = {
        title: graph.hostname + " <br>" + graph.name + " history",
        labels: ["time"], // Time is the first, the rest are dynamical.
        // drawAxesAtZero: true,
        // includeZero: true,
        fillGraph: true,
        strokeWidth: 1.2,
        series: {
            Y1: { fillAlpha: 0.1},
            Y2: { fillAlpha: 0.1}
        },
        labelsDiv: "label",
        labelsSeparateLines: true,
        legend: "always",
        hideOverlayOnMouseOut: false,
        labelsKMB: true,
        // connectSeparatedPoints: true, // Not working in Dygraph 2.1.0
    };
    zd.options.labelsDiv = "label_" + id;
    graph.items.forEach(item => zd.options.labels.push(item.name));
    return zd;
}

function DygraphTrend(graph, id) {
    const zd = {
        data: [],
        pre: {},
    };
    graph.items.forEach((item, idx) => {
        item.trend.forEach(trend => {
            let clock = trend.clock * 1e3;

            if (!zd.pre[clock]) {
                let ts = new Date();
                ts.setTime(clock);
                zd.pre[clock] = [ts];
            }
            // padding
            while (zd.pre[clock].length < idx + 1) {
                zd.pre[clock].push(null);
            }
            zd.pre[clock].push([
                +trend.value_min,
                +trend.value_avg,
                +trend.value_max,
            ])
        })
    })
    
    for (let k in zd.pre) {
        // padding
        while (zd.pre[k].length < graph.items.length + 1) {
            zd.pre[k].push([null,null,null]);
        }
        zd.data.push(zd.pre[k]);
    }
    delete zd.pre;
    // console.log(JSON.stringify(zd,null,2))
    zd.options = {
        title: graph.hostname + " <br>" + graph.name + " trend",
        titleHeight: 20,
        // xlabel: "time",
        // ylabel: graph.items[0].name,
        // y2label: graph.items[1].name,
        labels: [
            "time",
            // graph.items[0].name + " trend",
            // graph.items[1].name + " trend",
        ],
        // showRoller: true,
        drawAxesAtZero: true,
        includeZero: true,
        // fillGraph: true,
        // series: {
        //     Y1: { fillAlpha: 0.9 },
        //     Y2: { fillAlpha: 0.1 }
        // },
        labelsDiv: "labels",
        labelsSeparateLines: true,
        legend: "always",
        hideOverlayOnMouseOut: false,
        // labelsKMG2: true,
        labelsKMB: true,
        // showRangeSelector: true,
        customBars: true,
    };
    zd.options.labelsDiv = "label_" + id;
    graph.items.forEach(item => zd.options.labels.push(item.name));
    return zd;
}

export { DygraphHistory, DygraphTrend }