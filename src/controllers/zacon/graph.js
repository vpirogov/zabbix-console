function GetItems(graphitems) {
   let items = [];
   graphitems.forEach(gitem => {
    items.push(gitem.itemid)
   })
   return items;
}
function FormatGraph(graph) {
    let g = graph.graph[0];
    g.started = graph.started;
     // Set hostname.
     g.hostname = g.hosts[0].name ? g.hosts[0].name : g.hosts[0].host;
     if (!g.hostname) {
         g.hostname = g.hosts[0].hostid;
     }
     delete g.hosts;
     // Set items.
    g.items = [];
    graph.graphitem.forEach(gitem => {
        g.items.push(gitem)
    })
    graph.items.forEach(item => {
        g.items.forEach(gitem => {
            if (item.itemid === gitem.itemid) {
                gitem.name = item.name;
            }
        })
    });
 
    ["history", "trend"].forEach(valType => {
        if (!graph[valType]) return;
        graph[valType].forEach(qwe => {
            g.items.forEach(item => {
                if (qwe[0].itemid === item.itemid) {
                    item[valType] = qwe;
                }
            })
        })
    })
    return g;
}

export { GetItems, FormatGraph }