const severity = {
  0: "Not Classified",
  1: "Information",
  2: "Warning",
  3: "Average",
  4: "High",
  5: "Disaster",
}

function FormatHost(host) {
  TriggerFired(host.triggers);
  TriggerInfo(host);
  SNMPifAliases(host);
  setIP(host);
}

function setIP(host) {
  for (let i = 0; host.interfaces.length; i++) {
    if (host.interfaces[i].ip) {
      host.ip = host.interfaces[i].ip;
      return
    }
  }
  host.ip = "";
}

function TriggerInfo(host) {
  if (host.triggers) {
    host.triggerInfo = triggerInfo(host.triggers);
  }
  delete host.triggers;
}

function triggerInfo(triggers) {
  const i = {};
  i.total = 0;
  i.fired = 0;
  i.maxSeverityCode = 0;
  triggers.forEach(trigger => {
    if (trigger.status === "0") {
      i.total++;
      if (trigger.value === "1") {
        i.fired++;
        if (i.maxSeverityCode < trigger.priority) {
          i.maxSeverityCode = trigger.priority;
        }
      }
    }
  })
  i.maxSeverity = severity[i.maxSeverityCode];
  return i;
}

function TriggerFired(triggers) {
  triggers.forEach(trigger => {
    if (trigger.value === "1" && trigger.status === "0") {
      trigger.fired = true;
    } else {
      trigger.fired = false;
    }
  })
}

function SNMPifAliases(host) {
  if (!host.graph) return;
  host.graphs.forEach(graph => {
    graph.items.forEach(gitem => {
      for (let i = 0; i < host.items.length; i++) {
        if (
          gitem.type === "4"
          && host.items[i].type === "4"
          && equalSNMPId(gitem.key_, host.items[i].key_)
          ) {
            graph.description = host.items[i].lastvalue;
            return;
        }
      }
    })
    delete graph.items;
  })
  delete host.items;
}
function equalSNMPId(string1, string2) {
  let arr1 = string1.split('"');
  let arr2 = string2.split('"');
  return arr1.length === 3 && arr2.length === 3 && arr1[1] === arr2[1];
}
export {FormatHost}
