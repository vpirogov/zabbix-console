import { Zabbix } from "../zabbix/zabbix.js";
import dumpTmpl from "../../views/dump.hbs";
import groupTmpl from "../../views/group.hbs";
import hostsTmpl from "../../views/hosts.hbs";
import hostTmpl from "../../views/host.hbs";
import graphTmpl from "../../views/graph.hbs";
import helpTmpl from "../../views/help.hbs";
import { FormatHost } from "./host.js";
import { GetItems, FormatGraph } from "./graph.js";
import { DygraphTrend, DygraphHistory } from "./zabbixDygraph.js";
import Dygraph from "dygraphs";


export class ZaCon extends Zabbix {
  constructor(target, ...args) {
    super(...args);
    this.target = target;
    this.errorHandlers = [ data => this.write(data) ];
    this.successHandlers = []; // prune Zabbix console.log
    this.graphs = [];
  }
  schedule(entity, value) {
    switch (entity) {
      case "group":
        this.getGroupHosts(value);
        break;
      case "host":
        this.getHost(value);
        break;
      case "history":
        this.getGraph(value, this._getHistory(1))
          .then(result => this.showGraph(result, DygraphHistory));
        break;
      case "history3d":
        this.getGraph(value, this._getHistory(Date.now()* 1e-3 - 3*24*3600))
          .then(result => this.showGraph(result, DygraphHistory));
        break;
      case "trend4m":
        this.getGraph(value, this._getTrend(Date.now()* 1e-3 - 130*24*3600)) // last quarter
          .then(result => this.showGraph(result, DygraphTrend));
        break;
      case "trend":
        this.getGraph(value, this._getTrend(1)) // last quarter
        .then(result => this.showGraph(result, DygraphTrend));
        break;
      default:
        console.log(`not scheduled "${entity}" and "${value}"`);
    }
  }

  test () {
    // this.process("host", "10493");
    // this.ask("item.get", {
    //   hostids: "10493",
    //   output: ["name","key_", "lastvalue"],
    //   searchWildcardsEnabled: true,
    //   search: {
    //     "key_": "ifAlias*"
    //   }
    // })
    //   .then(result => this.write(result)
    this.schedule("host", "10493")
  }
  _getGraph (graphid, target) {
    target.started = new Date(); // to measure delay
    return Promise.all([ // 1. simultaneous API calls for graph and graphitems
      this.ask("graph.get", {
        "graphids" : graphid,
        "output": ["graphid", "name"],
        "selectHosts": ["host", "name"],
      }),
      this.ask("graphitem.get", {
        "graphids": graphid,
        "output": ["gitemid", "itemid", "drawtype", "sortorder"],
      })
    ])
      .then(result => { // 2. save results
        // Save in graph object.
        target.graph = result[0].result; // graph.get
        target.graphitem = result[1].result; // graphitem.get
        return target // result not needed
      })
  }
  _getHistory(from) {
    return item => {
      return this.ask("history.get", {
        "itemids": item,
        "output": "extend",
        "time_from": from,
      });
    }
  }
  _getTrend(from) {
    return item => {
      return this.ask("trend.get", {
        "itemids": item,
        "output": ["itemid", "clock", "value_avg", "value_min", "value_max"],
        "time_from": from,
      })
    }
  }
  getGraph(graphid, getValues) {
    let graph = {} // main data structure
    return this._getGraph(graphid, graph) // 1. & 2.
      .then(() => { // 3. simultaneous calls for items name and history
        const items = GetItems(graph.graphitem);
        // Ask for items names.
        let promises = [
          this.ask("item.get",{
            "itemids": items,
            "output": ["name"],
          })].concat(items.map(getValues)); // KISS
        return Promise.all(promises)
      })
      .then(result => { // 4. save results
        // Save items
        graph.items = result[0].result; 
        // Save history or trend
        const valType = graphType(result[1].result[0]);
        graph[valType] = []; // 
        for (let i = 1; i < result.length; i++) {
          graph[valType].push(result[i].result)
        }
        // Rafactor graph
        graph = FormatGraph(graph);
        // Set duration
        const received = new Date();
        graph.duration = received - graph.started;

        return graph;
      })
  }
  
  getHost(hostid) {
    const started = new Date();
    return Promise.all([
      this.ask("host.get", {
        hostids: hostid,
        output: ["hostid", "name", "description"],
        sortfield: "name",
        selectTriggers: ["description", "value", "lastchange", "status", "priority"],
        selectInventory: ["alias", "name", "os_full", "hardware", "location"],
        selectInterfaces: ["ip", "interfaceid"],
        active: true,
      }),
      this.ask("graph.get",{
        hostids: hostid,
        output: ["graphid", "name", "flags"],
        selectItems: ["name","key_", "lastvalue", "type"],
      }),
      this.ask("item.get",{ // try to find interface descriptors
        hostids: hostid,
        output: ["name","key_", "lastvalue", "type"],
        searchWildcardsEnabled: true,
        search: {
          "key_": "ifAlias*"
        }
      }),
    ])
      .then(result => {
        const res = result[0].result[0];
        res.graphs = result[1].result;
        res.items = result[2].result;
        FormatHost(res);
        const received = new Date();
        res.duration = received - started;
        return res
      })
      .then(result => {
        this.write(result,hostTmpl);
      })
    }
   getGroupHosts(groupid) {
    return this.ask("host.get", {
      groupids : [groupid],
      output : ["hostid", "name", "description"],
      sortfield : "name",
      selectInventory: ["location"],
      selectInterfaces: ["ip"],
      selectTriggers : ["description", "value", "lastchange", "only_true", "status", "priority"],
    })
    .then(result => {
      result.result.forEach(res => {
        FormatHost(res);
      })
      return result
    })
    .then(result => {
      this.write(result, hostsTmpl);
    })
  }
  getGroups() {
    return this.ask("hostgroup.get", {
      output: "extend",
      monitored_hosts: true,
      sortfield: "name"
    })
    .then(result => {
      this.write(result, groupTmpl)
    })
  }
  write(obj, Tmpl = dumpTmpl) {
    const elem = document.createElement("div");
    elem.innerHTML = Tmpl(obj);
    this.target.prepend(elem);
    window.scrollTo(0, 0);
  }
  showGraph(graph, dygrapher) {
    let id = "g" + Date.now() // must not begin with a digit
    const metadata = {
      duration: graph.duration,
      id,
      graphHeight: this.getGraphHeight(),
    }
    const elem = document.createElement("div");
    elem.innerHTML = graphTmpl(metadata);
    let zd = dygrapher(graph, id);
    this.target.prepend(elem);
    const target = document.querySelector(`div#${id}`)
    const g = new Dygraph(target, zd.data, zd.options);
    this.graphs.push(g);
    window.scrollTo(0, 0);
  }
  setGraphHeight(value) {
    if (!value) return;
    let height = parseInt(value,10);
    if (height < 100 || height > 1000) {  // check limits
      console.warn(`Invalid graph height: "${height}"`);
      return;
    }
    localStorage.setItem("graphHeight", value); // save
    // update existent graphs
    const elems = document.querySelectorAll("div.chart");
    for (let i = 0; i < elems.length; i++) {
      elems[i].style.height = height + "px"; // set outer div height
    }
    this.graphs.forEach(graph => graph.resize()); // fit graph to parent div
  }
  getGraphHeight() {
    return localStorage.getItem("graphHeight") || 160;
  }
  _sessionid(id) {
    if (id) {
      localStorage.setItem("sessionid", id);
    }
    return localStorage.getItem("sessionid");
  }
  moveUp() {
    const position = Math.floor(window.pageYOffset);
    let YCoords = [].map.call(this.target.children, elem => elem.offsetTop);
    let goto = 0;
    for (let i = 0; i < YCoords.length; i++) {
      if (position <= YCoords[i]) {
        window.scroll(0,goto);
        break;
      } else {
        goto = YCoords[i];
      }
    }
  }
  moveDown() {
    const position = Math.floor(window.pageYOffset);
    let YCoords = [].map.call(this.target.children, elem => elem.offsetTop);
    for (let i = 0; i < YCoords.length; i++) {
      if (position < YCoords[i] && YCoords[i] - position > 10) {
        window.scroll(0,YCoords[i]);
        break;
      }
    }
  }
  destroyClosest() {
    const position = Math.floor(window.pageYOffset);
    let messages = this.target.children;
    for (let i = 0; i < messages.length; i++) {
      if (position - messages[i].offsetTop < 10
        || position < messages[i].offsetTop) {
        messages[i].remove();
        break;
      }
    }
  }
  destroyAll() {
    while (this.target.children.length) {
      this.target.firstChild.remove();
    }
  }

  // As default successHandlers was overwritten, we have to
  // set new visualization functions for the parent methods.
  ping() {
    super.ping()
      .then(result => {
        this.write(result);
      })
  }
  login() {
    super.login()
      .then(result => {
        this.write(result);
      })
  }
  logout() {
    super.logout()
      .then(result => {
        this.write(result);
      })
  }
  help() {
    this.write(null, helpTmpl);
  }
}


function graphType(graphDataPiece) {
  if (graphDataPiece.value_avg) {
    return "trend"
  }
  return "history"
}