export class ZabbixFetchObj {
  constructor() {
    this.method = "POST";
    this.headers = { "Content-Type": "application/json-rpc" };
    this.body = {
      jsonrpc: "2.0",
      method: "",
      params: {},
      id: 627,
    }
  }
  fetchObj() {
    return {
      "method": this.method,
      "headers": this.headers,
      "body": JSON.stringify(this.body)
    }
  }
  set(method, params = {}) {
    this.body.method = method;
    this.body.params = params;
    return this;
  }
}
