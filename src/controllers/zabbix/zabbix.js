import { ZabbixFetchObj } from "./zabbixFetchObj.js";

export class Zabbix extends ZabbixFetchObj {
  constructor(url, user, password) {
    super();
    this.url = url;
    this.user = user;
    this.password = password;
    // An array of default error handlers
    // should be overwritten by user: [ f(error){}, ... ]
    this.errorHandlers = [ error => console.log(error.message)];
    // An array of default success handlers
    // should be overwritten by user: [ f(result){}, ... ]
    this.successHandlers = [ message => console.log(JSON.stringify(message)) ];
  }
  ask(method, params = {}) {
    this.body.auth = this._sessionid();
    this.set(method, params);
    return this._fetch();
  }
  ping() {
    delete this.body.auth;
    this.set("user.checkAuthentication", {"sessionid" : this._sessionid()});
    return this._fetch();
  }
  login() {
    delete this.body.auth;
    return this.set("user.login", {
      user: this.user,
      password: this.password
    })._fetch().then(result => {
      if (result.result) {
        this._sessionid(result.result);
      }
      return result;
    });
  }
  logout() {
    return this.ask("user.logout");
  }
  _fetch() {
    const started = new Date();
    return fetch(this.url,this.fetchObj())
      .then(result => result.json())
      .then(result => {
        const received = new Date();
        result.duration = received - started;
        return result;
      })
      .then(result => {
        this.successHandlers.forEach(fn => {
          fn(result);
        });
        return result;
      })
      .catch(error => {
        this.errorHandlers.forEach(fn => {
          fn(error);
        });
      })
  }
  _sessionid(id) {
    if (id) {
      localStorage.setItem("sessionid", id);
    }
    return localStorage.getItem("sessionid");
  }
}
