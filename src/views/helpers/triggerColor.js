module.exports = function triggers(priority) {
  switch (priority) {
    case "5":
      return "w3-black";
    case "4":
      return "w3-red"
    case "3":
      return "w3-orange"
    case "2":
      return "w3-amber"
    case "1":
      return "w3-khaki"
    case "0":
      return "w3-white"
  }
}
