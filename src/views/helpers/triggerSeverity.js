module.exports = function triggers(priority) {
  switch (priority) {
    case "5":
      return "Disaster";
    case "4":
      return "High"
    case "3":
      return "Average"
    case "2":
      return "Warning"
    case "1":
      return "Information"
    case "0":
      return "Not classified"
  }
}
