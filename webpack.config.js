const path = require('path');
// const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  plugins: [
    // new CleanWebpackPlugin(),
  ],
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist',
  },
  module: {
    rules: [
      {
       test: /\.js$/,
       exclude: "/node_modules/",
       use: {
         loader: "babel-loader",
         options: {
           presets: ["@babel/preset-env"]
         }
       }
     },
     {
       test: /\.css$/,
       exclude: /node_modules/,
       use: [
         'style-loader',
         'css-loader',
       ],
     },
     {
       test: /\.hbs$/,
       exclude: /node_modules/,
       use: [{
         loader: "handlebars-loader",
         options: {
           helperDirs: path.join(__dirname, "src", "views", "helpers")
         //   precompileOptions: {
         //     knownHelpersOnly: false,
         //   },
         }
       }]
     }
    ]
  }
};
